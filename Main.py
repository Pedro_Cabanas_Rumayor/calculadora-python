from calculadora import Calculadora
from calculadora_test import CalculadoraTest


def main():
    numero1 = int(input('Primer número: '))
    numero2 = int(input('Segundo número: '))
    operacion = input('Tipo de operación(suma, resta o mult): ')

    calcular =  Calculadora(numero1, numero2, operacion)

    print(calcular.operar())
    calculadoraParaTest= CalculadoraTest()
    calculadoraParaTest.testOperarSuma()
main()
