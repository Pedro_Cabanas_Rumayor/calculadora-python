from calculadora import Calculadora

class CalculadoraTest():
    def testOperarSuma(self):
        numero1_test= int(2)
        numero2_test= int(4)
        operacionSuma_test= 'suma'

        Calculadora_test= Calculadora(numero1_test, numero2_test, operacionSuma_test)
        resultadoSuma= numero1_test + numero2_test
        assert Calculadora_test.operar() == resultadoSuma
