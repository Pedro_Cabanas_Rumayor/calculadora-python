class Calculadora:
    def __init__(self, numero1, numero2, operacion):
        self.numero1 = numero1
        self.numero2 = numero2
        self.operacion = operacion

    def operar(self):
        result= None
        if self.operacion == "suma":
            result = self.numero1+self.numero2
        elif self.operacion == "resta":
            result = self.numero1-self.numero2
        elif self.operacion == "mult":
            result = self.numero1*self.numero2
        else:
            print("Operación no reconocida")
        return result